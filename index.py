import sys
from pathlib import Path
import pandas as pd
from urllib.request import urlopen
import json

# Check that the user provided an input file name
if len(sys.argv) < 2:
    print('Please provide an output location')
    print('Example:\tpython index.py ./data')
    sys.exit(1)

# Read the output location from arguments
output_folder = sys.argv[1]

# URL where we find the URL to the CSV file
url = (
    "https://opendata.stadt-muenster.de/api/3/action/resource_show?id=klimadashboard"
)

# store the response of URL
response = urlopen(url)

# storing the JSON response
# from url in data
data_json = json.loads(response.read())

# reading the URL of the CSV file
csv_url = data_json["result"]["url"]

# # Read in the CSV file
df = pd.read_csv(csv_url, delimiter=";")

# read all file named from the DATEINAME column
file_names = df["DATEINAME"].unique()

parse_success = 0
parse_fail = 0

# iterate over file names
for file_name in file_names:
    # filter file_name data
    file_name_df = df[df["DATEINAME"] == file_name]

    # check if file is not undefined
    if "unbekannt" in file_name_df["ZEIT"].values:
        # print that the filename could not be processed
        print("Could not process file: " + file_name)
        parse_fail += 1
        continue

    # Replace commas with dots and convert values to float
    file_name_df = file_name_df.assign(WERT = file_name_df["WERT"].str.replace(",", ".").astype(float))

    file_name_df.drop_duplicates(subset=["ZEIT", "MERKMAL"], keep="first", inplace=True)

    # Pivot the DataFrame to reshape it
    df_pivot = file_name_df.pivot(
        index="ZEIT", columns="MERKMAL", values="WERT"
    )

    # reset the index to keep year values
    df_pivot = df_pivot.reset_index()

    # Create output directory if it does not exist
    p = Path(output_folder)
    p.mkdir(exist_ok=True)

    # Write resulting DataFrame to CSV
    df_pivot.to_csv(p / (file_name + ".csv"), index=False)

    parse_success += 1

print()
print(f"Parse success: \t{parse_success}")
print(f"Parse fail: \t{parse_fail}")
