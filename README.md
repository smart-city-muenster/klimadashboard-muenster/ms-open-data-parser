# Script: CSV Parser

This Python script downloads a CSV file from a specified URL, processes it, and outputs the result to a specified output directory. It uses the pandas library to read and manipulate CSV files.

## Usage

The script takes one argument, which is the output directory. For example, to output the result to a directory named "output", run the following command:

```
python csv_parser.py output
```

## Input

The input file is a CSV file with the following columns:

- DATEINAME: the name of the file
- ZEIT: the time
- MERKMAL: the feature
- WERT: the value of the feature at the specified time

The CSV file is downloaded from the following URL: `https://opendata.stadt-muenster.de/api/3/action/resource_show?id=testdatei&page=0`.

## Processing

The script reads the input CSV file and performs the following operations:

1. Filter the data by filename
2. Check if the file is undefined
3. Replace commas with dots and convert values to float
4. Pivot the DataFrame to reshape it
5. Reset the index to keep year values
6. Write the resulting DataFrame to CSV

## Output

The output is a CSV file for each unique filename in the input CSV file. The output CSV file has the following columns:

- ZEIT: the time
- MERKMAL_1: the value of feature 1 at the specified time
- MERKMAL_2: the value of feature 2 at the specified time
- ...

The output CSV files are saved in the specified output directory. If the output directory does not exist, it is created.

## Error Handling

If the script encounters an undefined file, it skips the file and outputs an error message to the console.

## Dependencies

The script requires the following libraries:

- pandas
- pathlib
- urllib.request
- json

These libraries can be installed using pip:

```
pip install -r requirements.txt
```

### Using a Devcontainer

If you're using Visual Studio Code, you can take advantage of the included devcontainer file to develop and run the script inside a Docker container. To use the devcontainer, make sure you have the [Remote - Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) installed in Visual Studio Code.

To use the devcontainer, follow these steps:

1. Clone this repository to your local machine.
2. Open the repository folder in Visual Studio Code.
3. Press `F1` or `Ctrl+Shift+P` to open the Command Palette.
4. Type `Remote-Containers: Reopen in Container` and select the command.
5. Wait for the container to build (this may take a few minutes the first time).
6. Once the container is ready, you can use the terminal in Visual Studio Code to run the script.

The included devcontainer file installs the necessary dependencies and sets up the development environment inside the container. If you need to install additional dependencies, you can add them to the `requirements.txt` file and rebuild the container.


## Authors

- Felix Erdmann

## License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).

